Rails.application.routes.draw do
  get 'companys/index'

  resources :companys do
    collection do
      get :destroy_all
    end
  end

  root 'companys#index'
end
