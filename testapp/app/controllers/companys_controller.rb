class CompanysController < ApplicationController

  def index
    @companys = Company.all
  end

  def new
    @company = Company.new
  end

  def create
    response = HTTParty.get('https://iextrading.com/api/1.0/stock/market/collection/list?collectionName=in-focus')
      comp = JSON.parse response.body
      id=0
      comp.each do |row|
        id+=1
        name=row['companyName'].inspect
        price=row['latestPrice'].inspect+'$'
        params[:company]= {"name"=>name, "price"=>price}
        @company = Company.new(params.require(:company).permit(:name, :price))
        @company.save
      end
  end

  def destroy_all
    @companys = Company.all
    @companys.destroy_all
    redirect_to "/companys"
  end

  def destroy
    @company = Company.find(params[:id])
    @company.destroy

    redirect_to "/companys"
  end

end

